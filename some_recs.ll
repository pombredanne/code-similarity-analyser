; ModuleID = 'some_recs.bc'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; Function Attrs: nounwind uwtable
define i32 @f(i32 %n) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  store i32 %n, i32* %2, align 4
  %3 = load i32* %2, align 4
  %4 = icmp sgt i32 %3, 20
  br i1 %4, label %5, label %8

; <label>:5                                       ; preds = %0
  %6 = load i32* %2, align 4
  %7 = call i32 @g(i32 %6)
  store i32 %7, i32* %1
  br label %12

; <label>:8                                       ; preds = %0
  %9 = load i32* %2, align 4
  %10 = load i32* %2, align 4
  %11 = mul nsw i32 %9, %10
  store i32 %11, i32* %1
  br label %12

; <label>:12                                      ; preds = %8, %5
  %13 = load i32* %1
  ret i32 %13
}

; Function Attrs: nounwind uwtable
define i32 @g(i32 %n) #0 {
  %1 = alloca i32, align 4
  store i32 %n, i32* %1, align 4
  %2 = load i32* %1, align 4
  %3 = load i32* %1, align 4
  %4 = call i32 @f(i32 %3)
  %5 = add nsw i32 %2, %4
  ret i32 %5
}

; Function Attrs: nounwind uwtable
define i32 @h(i32 %n) #0 {
  %1 = alloca i32, align 4
  store i32 %n, i32* %1, align 4
  %2 = load i32* %1, align 4
  %3 = call i32 @f(i32 %2)
  %4 = call i32 @g(i32 %3)
  ret i32 %4
}

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4
  store i32 0, i32* %1
  %2 = call i32 @h(i32 4)
  %3 = call i32 @h(i32 3)
  %4 = call i32 @g(i32 %3)
  ret i32 0
}

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5.0 (212765)"}
