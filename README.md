# README #

Code similarity analysis pass in LLVM . Honours 4th year project by Bilyan Borisov, all rights reserved, not to be redistributed. You are encouraged to browse the code through the website or clone it locally and look at it but you need my explicit consent to modify, adapt, or otherwise use it for your own intentions. That being said, this is still work in progress - expect everything to change.

The LLVM opt pass I am producing is in llvm-3.5/lib/Transforms/CodeSimilarityAnalyser .
To run the pass, compile a file to bitcode with

    clang -emit-llvm -o file.bc file.c

and then run

    opt -analyze -indvars   -load llvm-3.5/Debug+Asserts/lib/CodeSimilarityAnalyser.so  -codesimilarityanalyser  file.bc > /dev/null

You would need llvm3.5 and clang binaries built on your path as well as a locally built llvm3.5 with Debug + Asserts.

There is a presentation folder with some slides if you wish to see a very general overview of the project - go to presentations/ and look for a file called presentation.pdf for more info

Please also note that I was made aware of some brand new research in the current topic and I would be incorporating that into my work further on - this goes in-line with the notice that this is complete work in progress and things might change overnight. 