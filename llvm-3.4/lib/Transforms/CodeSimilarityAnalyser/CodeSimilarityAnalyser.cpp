/**
 * Loop analysing program Written by Bilyan Borisov
 */
#include "llvm/Analysis/CFGPrinter.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopInfoImpl.h"
#include "llvm/Analysis/LoopIterator.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/DebugInfo.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/DebugLoc.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/SimplifyIndVar.h"
#include "llvm/Transforms/Utils/LoopUtils.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <map>
#include <utility>
using namespace llvm;

namespace {

  typedef struct {
    size_t num_inst;
    std::vector<std::string> opcodes;
    std::vector<std::string> insts;
  } loop_info;
  
  struct CodeSimilarityAnalyser : public ModulePass{
    static char ID;
    CodeSimilarityAnalyser() : ModulePass(ID){
      errs() << "Init called\n";
      loops_infos = std::vector<std::pair<Loop*,loop_info>>();
    }

    std::vector<std::pair<Loop*,loop_info>> loops_infos;

    void getAnalysisUsage(AnalysisUsage &AU) const {
      AU.setPreservesAll();
      AU.addRequired<LoopInfo>();
      AU.addPreserved<LoopInfo>();
      AU.addRequiredID(LoopSimplifyID);
      AU.addPreservedID(LoopSimplifyID);
    }
    virtual bool runOnModule(Module& Mod){
      size_t num_inst = 0;
      std::vector<std::string> opcodes;
      std::vector<std::string> insts;
      errs() << "info\n";
      for(Module::iterator f = Mod.begin(), fend = Mod.end(); f != fend; ++f){
        if ((*f).isDeclaration()) continue;
        LoopInfo& LI = getAnalysis<LoopInfo>(*f);
        for(LoopInfo::iterator l = LI.begin(), e = LI.end(); l != e; ++l){
          for(Loop::block_iterator i = (*l)->block_begin(); i != (*l)->block_end(); ++i){
            num_inst += (*i)->size();
            DEBUG(errs() << (*i)->getName() << "\n");
            for(BasicBlock::iterator j = (*i)->begin(); j != (*i)->end(); ++j){
              opcodes.push_back(j->getOpcodeName());
            }
          }
          loop_info li{num_inst, opcodes, insts};
          loops_infos.push_back(std::make_pair(*l, li));
        }
      }
      // pointers are dangling with current setup
      errs() << "Time to compute similarities \n";
      errs() << "Number of loops: " << loops_infos.size() <<"\n";
      int ls = loops_infos.size();
      std::vector<std::vector<double>> M(ls);
      for(int i = 0; i < ls; ++i){
        M.at(i) = std::vector<double>(ls);
      }
      int i, j = 0;
      double max_sim = 0.0;
      double avg_sim = 0.0;
      double avg_sim2 = 0.0;
      int iloops = 0;
      std::map<int,int> hist;
      std::pair<Loop*, Loop*> max_loops;
      std::vector<double> buckets{0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0};
      int count = 0;
      for(auto l: loops_infos){
        for(auto p: loops_infos){
          if (l.first != p.first && j > i){
            double sim = analyse_similarity_lcs(l.second,p.second);
            if (sim > max_sim && sim != 1.0){ // empty loops similarity of 1?
              max_sim = sim;
              max_loops = std::make_pair(l.first, p.first);
            }
            if(sim == 1.0) ++iloops;
            for(unsigned b = 0; b < buckets.size(); ++b){
              if(buckets[b] <= sim && sim < buckets[b+1]){
                ++hist[b];
                break;
              }
            }
            avg_sim += sim;
            avg_sim2 += sim*sim;
            count++;
            M.at(i).at(j) = sim;
            double mean = avg_sim / count;
            errs() << "similarity: " << sim <<" max_sim: " << max_sim << " mean_sim: " << mean << " stdd_sim: " << std::sqrt((avg_sim2 / count) - mean*mean) <<  "\n";
          }
          ++j;
        }
        j = 0;
        ++i;
      }
      Loop* L = max_loops.first;
      //DebugLoc dl = L->getStartLoc();

      for (auto p : hist) {
        errs() << p.first << ' ' << std::string(p.second/200, '*') << '\n';
      }
      return false;
    }
    double analyse_similarity_lcs(loop_info li1, loop_info li2){
      size_t n = li1.num_inst;
      size_t m = li2.num_inst;
      std::vector<std::string> opcodes1 = li1.opcodes;
      std::vector<std::string> opcodes2 = li2.opcodes;
      // LCS
      DEBUG(errs() << "n: " << n << "\n");
      DEBUG(errs() << "m: " << m << "\n");
      DEBUG(errs() << "num ops n: "<< opcodes1.size() << "\n");
      DEBUG(errs() << "num ops m: "<< opcodes2.size() << "\n");
      std::vector<std::vector<int>> C(m+1);
      for(size_t i = 0; i <= m; ++i){
        C.at(i) = std::vector<int>(n+1);
      }
      // all elements are allready 0
      for (size_t i = 1; i <= m; ++i){
        for (size_t j = 1; j <= n; ++j){
          if(opcodes2.at(i-1).compare(opcodes1.at(j-1)) == 0){
            C.at(i).at(j) = C.at(i-1).at(j-1) + 1;             
          }
          else{
            C.at(i).at(j) = std::max(C.at(i).at(j-1),C.at(i-1).at(j));
          }
        }
      } 
      double nom = (double) 2*C.at(m).at(n);
      double denom = (double) opcodes1.size() + opcodes2.size();
      return nom/denom;
      return 0;
    }
    double analyse_similarity_lcss(loop_info li1, loop_info li2){
      
      return 0.1;
    }
    void printLoop(std::vector<std::string> v){
      for(auto x: v){
        errs() << x;
      }
    }
  };
}

char CodeSimilarityAnalyser::ID = 0;
static RegisterPass<CodeSimilarityAnalyser> X("codesimilarityanalyser", "Code Similarity Analyser Pass", false, false);
//asd
