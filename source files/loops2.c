int main()
{
	int x = 0;
	int y = 0;
	// Two loops that are executed the same amount of times
	// but have different bodies
	for(int i = 0; i < 10; ++i){
		x += x + 1;
	}
	for(int j = 0; j < 10; ++j){
		y += y + 2;
	}
}